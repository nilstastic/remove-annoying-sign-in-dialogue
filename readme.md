# Remove annying overlay
A Chrome plugin that removes the annoying overlay that is presented when you are in a meeting and need to relogin again. This makes it possible to continue the meeting and not get annoyed.

## Installation
1. Go to chrome://extensions/
2. Select load unpacked
3. Select the folder with the extension